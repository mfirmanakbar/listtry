package com.nexsoft.firman.listtry;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class ListClick2DActivity extends AppCompatActivity {

    ListView list;
    AdapterClick2 adapter;
    List<dataClick2> itemList = new ArrayList<dataClick2>();

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    int itemTotal;
    int no_yg_diklik;
    Boolean status_awal=true;

    String dataTxt="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_click2);

        list    = (ListView) findViewById(R.id.listViewClick2);

        adapter = new AdapterClick2(ListClick2DActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                no_yg_diklik = itemList.get(position).getNo();
                status_awal=false;
                loadClick2Data2();
            }
        });

        dataTxt = "0|National|60|End^1|RBH-1|50|End^1|JAKARTA GREATER|10|End^2|JAKARTA GOOD |80|End^2|TANGERANG|38|End^3|TANGERANG GT|20|End^3|PT LARISTAMA SEJAHTERA (GT)|63|End^3|JAKARTA MT|29|End^4|JAKARTA MX|95|End^4|PT LARISTAMA SEJAHTERA (MT)|34|End^2|JAKARTA WEST|47|End^1|JAKARTA GC|86|End^0|DWITUNGGAL CITRA ARYAGUNA|46|End^";
        if (dataTxt!=""){
            loadClick2Data2();
        }

    }

    private void loadClick2Data2() {

        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        itemTotal = itemInduk.length;

        if(status_awal==true){
            //munculin semua data 0
            adapter.items.clear();
            for (int a=0; a<itemInduk.length; a++){
                String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
                if(Integer.parseInt(itemAnak[0])==0){
                    dataClick2 ita = new dataClick2();
                    ita.setNo(Integer.parseInt(itemAnak[0]));
                    ita.setHuruf(itemAnak[1]);
                    ita.setAngka(Integer.parseInt(itemAnak[2]));
                    itemList.add(ita);
                    adapter.notifyDataSetChanged();
                }
            }
            status_awal=false;
        }else{
            //munculin semua data 1 dst

        }

    }


}
