package com.nexsoft.firman.listtry;

/**
 * Created by firmanmac on 3/9/17.
 */

public class dataClick2E {
    int ar_pk, ar_lokasi, ar_no, ar_angka;
    String ar_huruf;

    public dataClick2E() {
    }

    public dataClick2E(int ar_pk, int ar_lokasi, int ar_no, int ar_angka, String ar_huruf) {
        this.ar_pk = ar_pk;
        this.ar_lokasi = ar_lokasi;
        this.ar_no = ar_no;
        this.ar_angka = ar_angka;
        this.ar_huruf = ar_huruf;
    }

    public int getAr_pk() {
        return ar_pk;
    }

    public void setAr_pk(int ar_pk) {
        this.ar_pk = ar_pk;
    }

    public int getAr_lokasi() {
        return ar_lokasi;
    }

    public void setAr_lokasi(int ar_lokasi) {
        this.ar_lokasi = ar_lokasi;
    }

    public int getAr_no() {
        return ar_no;
    }

    public void setAr_no(int ar_no) {
        this.ar_no = ar_no;
    }

    public int getAr_angka() {
        return ar_angka;
    }

    public void setAr_angka(int ar_angka) {
        this.ar_angka = ar_angka;
    }

    public String getAr_huruf() {
        return ar_huruf;
    }

    public void setAr_huruf(String ar_huruf) {
        this.ar_huruf = ar_huruf;
    }
}
