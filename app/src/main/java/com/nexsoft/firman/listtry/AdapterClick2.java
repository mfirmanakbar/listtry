package com.nexsoft.firman.listtry;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by firmanmac on 3/7/17.
 */

public class AdapterClick2 extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    public List<dataClick2> items;

    public AdapterClick2(Activity activity, List<dataClick2> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item1,null);
        }

        TextView no = (TextView) convertView.findViewById(R.id.txtNo1);
        TextView huruf = (TextView) convertView.findViewById(R.id.txtHuruf1);
        TextView angka = (TextView) convertView.findViewById(R.id.txtAngka1);

        dataClick2 dataClick2 = items.get(position);
        int xno = dataClick2.getNo();
        String xhuruf = dataClick2.getHuruf();
        int xangka = dataClick2.getAngka();
        no.setText(xno+"");
        huruf.setText(xhuruf);
        angka.setText(xangka+"");

        return convertView;
    }
}
