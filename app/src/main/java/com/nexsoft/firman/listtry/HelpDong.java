package com.nexsoft.firman.listtry;

import android.content.res.Resources;

/**
 * Created by firmanmac on 3/10/17.
 */

public class HelpDong {
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
}
