package com.nexsoft.firman.listtry;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.CircleProgress;

import java.util.List;

/**
 * Created by firmanmac on 3/7/17.
 */

public class AdapterClick2F extends BaseAdapter {

    public static int xlayac, xlayec, xlaysku;

    private Activity activity;
    private LayoutInflater inflater;
    public List<dataClick2F> items;

    public AdapterClick2F(Activity activity, List<dataClick2F> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null){
            convertView = inflater.inflate(R.layout.item2f,null);
        }

        final TextView no = (TextView) convertView.findViewById(R.id.txtNo);
        final TextView nama = (TextView) convertView.findViewById(R.id.txtNama);
        final TextView ac = (TextView) convertView.findViewById(R.id.txtAc);
        final TextView ec = (TextView) convertView.findViewById(R.id.txtEc);
        final TextView sku = (TextView) convertView.findViewById(R.id.txtSku);

        final CircleProgress cp_ac = (CircleProgress) convertView.findViewById(R.id.circle_progressxAc);
        final CircleProgress cp_ec = (CircleProgress) convertView.findViewById(R.id.circle_progressxEc);
        final CircleProgress cp_sku = (CircleProgress) convertView.findViewById(R.id.circle_progressxSku);
        //ArcProgress arcProgress = (ArcProgress) convertView.findViewById(R.id.arc_progressx);
        cp_ac.setProgress(67);
        cp_ec.setProgress(94);
        cp_sku.setProgress(38);
        //arcProgress.setProgress(80);

        //get width
        int sw = (HelpDong.getScreenWidth() * 35) / 100;
        int sw2 = (HelpDong.getScreenWidth() * 5) / 100;
        int sw3 = (HelpDong.getScreenWidth() * 9) / 100;

        nama.setWidth(sw);
        no.setWidth(sw2);
        ac.setWidth(sw3);
        ec.setWidth(sw3);
        sku.setWidth(sw3);

        //get width text view
//        ViewTreeObserver vto = ac.getViewTreeObserver();
//        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                Layout layout = ac.getLayout();
//                Log.d("tvAC",""+layout.getWidth());
//            }
//        });

        dataClick2F datax = items.get(position);
        int xno = datax.getAr_no();
        String xnama = datax.getAr_nama();
        int xac = datax.getAr_ac();
        int xec = datax.getAr_ec();
        int xsku = datax.getAr_sku();

        no.setText(xno+"");
        nama.setText(xnama);
        ac.setText(xac+"");
        ec.setText(xec+"");
        sku.setText(xsku+"");

        final LinearLayout layac = (LinearLayout) convertView.findViewById(R.id.layAc);
        final LinearLayout layec = (LinearLayout) convertView.findViewById(R.id.layEc);
        final LinearLayout laysku = (LinearLayout) convertView.findViewById(R.id.laySku);


        return convertView;
    }
}
