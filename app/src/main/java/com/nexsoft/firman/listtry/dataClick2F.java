package com.nexsoft.firman.listtry;

/**
 * Created by firmanmac on 3/9/17.
 */

public class dataClick2F {
    int ar_pk, ar_no, ar_lokasi, ar_ac, ar_ec, ar_sku;
    String ar_nama;

    public dataClick2F() {
    }

    public dataClick2F(int ar_pk, int ar_no, int ar_lokasi, int ar_ac, int ar_ec, int ar_sku, String ar_nama) {
        this.ar_pk = ar_pk;
        this.ar_no = ar_no;
        this.ar_lokasi = ar_lokasi;
        this.ar_ac = ar_ac;
        this.ar_ec = ar_ec;
        this.ar_sku = ar_sku;
        this.ar_nama = ar_nama;
    }

    public int getAr_pk() {
        return ar_pk;
    }

    public void setAr_pk(int ar_pk) {
        this.ar_pk = ar_pk;
    }

    public int getAr_no() {
        return ar_no;
    }

    public void setAr_no(int ar_no) {
        this.ar_no = ar_no;
    }

    public int getAr_lokasi() {
        return ar_lokasi;
    }

    public void setAr_lokasi(int ar_lokasi) {
        this.ar_lokasi = ar_lokasi;
    }

    public int getAr_ac() {
        return ar_ac;
    }

    public void setAr_ac(int ar_ac) {
        this.ar_ac = ar_ac;
    }

    public int getAr_ec() {
        return ar_ec;
    }

    public void setAr_ec(int ar_ec) {
        this.ar_ec = ar_ec;
    }

    public int getAr_sku() {
        return ar_sku;
    }

    public void setAr_sku(int ar_sku) {
        this.ar_sku = ar_sku;
    }

    public String getAr_nama() {
        return ar_nama;
    }

    public void setAr_nama(String ar_nama) {
        this.ar_nama = ar_nama;
    }
}
