package com.nexsoft.firman.listtry;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ListClick2CActivity extends AppCompatActivity {

    ListView list;
    AdapterClick2 adapter;
    List<dataClick2> itemList = new ArrayList<dataClick2>();

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    int itemTotal;
    int itemGetNow;

    String dataTxt="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_click2);

        list    = (ListView) findViewById(R.id.listViewClick2);

        adapter = new AdapterClick2(ListClick2CActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(itemTotal!=itemGetNow){
                    int a = itemGetNow;
                    loadClick2Data2(a+1);
                }else {
                    Toast.makeText(ListClick2CActivity.this,"Data telah lengkap!",Toast.LENGTH_LONG).show();
                }
            }
        });

        itemGetNow = 1;
        loadClick2Data2(itemGetNow);

    }

    private void loadClick2Data2(int itemGetNowx) {
        dataTxt = "0|National|60|End^1|RBH-1|50|End^2|JAKARTA GREATER|10|End^3|JAKARTA GOOD |80|End^4|TANGERANG|38|End^5|TANGERANG GT|20|End^6|PT LARISTAMA SEJAHTERA (GT)|63|End^4|JAKARTA MT|29|End^5|JAKARTA MX|95|End^6|PT LARISTAMA SEJAHTERA (MT)|34|End^4|JAKARTA WEST|47|End^5|JAKARTA GC|86|End^6|DWITUNGGAL CITRA ARYAGUNA|46|End^";
        String[] items = dataTxt.toString().trim().split("\\^");
        itemTotal = items.length;
        if (itemGetNowx == 1 ){
            String[] items2 = items[itemGetNowx-1].toString().trim().split("\\|");
            dataClick2 item = new dataClick2();
            item.setNo(Integer.parseInt(items2[0]));
            item.setHuruf(items2[1]);
            item.setAngka(Integer.parseInt(items2[2]));
            itemList.add(item);
        }else{
            adapter.items.clear();
            itemGetNow = itemGetNowx;
            for (int a=0; a<itemGetNowx; a++){
                String[] itemss2 = items[a].toString().trim().split("\\|");
                dataClick2 item = new dataClick2();
                item.setNo(Integer.parseInt(itemss2[0]));
                item.setHuruf(itemss2[1]);
                item.setAngka(Integer.parseInt(itemss2[2]));
                itemList.add(item);
                adapter.notifyDataSetChanged();
            }
        }
    }


}
