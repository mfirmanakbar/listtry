package com.nexsoft.firman.listtry;

/**
 * Created by firmanmac on 3/7/17.
 */

public class dataClick2 {
    int no;
    String huruf;
    int angka;

    public dataClick2() {
    }

    public dataClick2(int no, String huruf, int angka) {
        this.no = no;
        this.huruf = huruf;
        this.angka = angka;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getHuruf() {
        return huruf;
    }

    public void setHuruf(String huruf) {
        this.huruf = huruf;
    }

    public int getAngka() {
        return angka;
    }

    public void setAngka(int angka) {
        this.angka = angka;
    }
}
