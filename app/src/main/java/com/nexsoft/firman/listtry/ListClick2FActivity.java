package com.nexsoft.firman.listtry;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListClick2FActivity extends AppCompatActivity {

    private String dataTxt="";

    private ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    private ArrayList<Integer> ar_no = new ArrayList<Integer>();
    private ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();
    private ArrayList<String> ar_nama = new ArrayList<String>();
    private ArrayList<Integer> ar_ac = new ArrayList<Integer>();
    private ArrayList<Integer> ar_ec = new ArrayList<Integer>();
    private ArrayList<Integer> ar_sku = new ArrayList<Integer>();

    private int no_skrg, pk;

    private ListView list;
    private AdapterClick2F adapter;
    private List<dataClick2F> itemList = new ArrayList<dataClick2F>();

    private int itemTotal, pk_yg_diklik, lokasi_yg_diklik=0;
    private Boolean status_awal=true;

    //private TextView txtInfoLokasi, txtInfoHistoriesF;
    private TextView txtInfoHistoriesF;
    private String txtInfoLokasi="";

    private String posisi_namaNow = "Home";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_click2_f);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //get width
        int sw = (HelpDong.getScreenWidth() * 35) / 100;
        int sw2 = (HelpDong.getScreenWidth() * 5) / 100;
        int sw3 = (HelpDong.getScreenWidth() * 9) / 100;

        final TextView tvNo = (TextView) findViewById(R.id.txtHeaderNo);
        final TextView tvNama = (TextView) findViewById(R.id.txtHeaderNama);
        final TextView tvAc = (TextView) findViewById(R.id.txtHeaderAc);
        final TextView tvEc = (TextView) findViewById(R.id.txtHeaderEc);
        final TextView tvSku = (TextView) findViewById(R.id.txtHeaderSku);
        tvNo.setWidth(sw2);
        tvNama.setWidth(sw);

        //Log.d("LAYSS", ""+adapter.xlayac+"-"+adapter.xlayec+"-"+adapter.xlaysku);
        tvAc.setWidth(172);
        tvEc.setWidth(172);
        tvSku.setWidth(172);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                if (!txtInfoLokasi.getText().toString().equals("0")){
                    kembaliKeHeader();
                }
            }
        });*/


        list    = (ListView) findViewById(R.id.listViewClick2F);
        //txtInfoLokasi = (TextView) findViewById(R.id.infoLokasiTxtF);
        txtInfoHistoriesF = (TextView) findViewById(R.id.infoHistoriesF);

        adapter = new AdapterClick2F(ListClick2FActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pk_yg_diklik = itemList.get(position).getAr_pk();
                lokasi_yg_diklik = itemList.get(position).getAr_lokasi();
                setHistoryUp(position);
                loadClick2Data2();
            }
        });

        dataTxt="0|National|60|34|43|End^1|RBH-1|50|10|90|End^1|JAKARTA GREATER|10|76|44|End^2|JAKARTA GOOD |80|28|62|End^2|TANGERANG|38|14|77|End^3|TANGERANG GT|20|22|33|End^3|PT LARISTAMA SEJAHTERA (GT)|63|66|52|End^3|JAKARTA MT|29|41|59|End^4|JAKARTA MX|95|83|19|End^4|PT LARISTAMA SEJAHTERA (MT)|34|24|16|End^1|JAKARTA GC|86|23|73|End^2|JAKARTA WEST|47|32|47|End^0|DWITUNGGAL CITRA ARYAGUNA|46|48|32|End^";
        if (dataTxt!=""){
            simpanKeArray();
            lihatDataArray();
            loadClick2Data2();
        }
        txtInfoLokasi = lokasi_yg_diklik+"";//txtInfoLokasi.setText(lokasi_yg_diklik+"");

    }

    private void setHistoryUp(int position) {
        if(cekDataPkLokasi()==true){
            txtInfoHistoriesF.setVisibility(View.VISIBLE);
            posisi_namaNow = posisi_namaNow+" > "+itemList.get(position).getAr_nama();
            txtInfoHistoriesF.setText(posisi_namaNow);
        }
    }

    private void loadClick2Data2() {
        setKeListView();
    }

    private void setKeListView() {
        if (status_awal==true){
            //muncul pertama
            adapter.items.clear();
            for (int a=0; a<ar_pk.size(); a++){
                if(ar_no.get(a)==0){
                    dataClick2F ita = new dataClick2F();
                    ita.setAr_pk(ar_pk.get(a));
                    ita.setAr_lokasi(ar_lokasi.get(a));
                    ita.setAr_no(ar_no.get(a));
                    ita.setAr_nama(ar_nama.get(a));
                    ita.setAr_ac(ar_ac.get(a));
                    ita.setAr_ec(ar_ec.get(a));
                    ita.setAr_sku(ar_sku.get(a));
                    itemList.add(ita);
                    adapter.notifyDataSetChanged();
                }
            }
            status_awal=false;
        }else {
            if (cekDataPkLokasi()==true){
                //muncul selanjutnya
                //String getInfoLokasi = txtInfoLokasi.getText().toString();
                txtInfoLokasi = txtInfoLokasi+"^"+pk_yg_diklik;

                adapter.items.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("TES",a+"");
                    if(ar_lokasi.get(a)==pk_yg_diklik){
                        Log.d("TES_TRUE_IF",ar_lokasi.get(a)+"");
                        dataClick2F ita = new dataClick2F();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_nama(ar_nama.get(a));
                        ita.setAr_ac(ar_ac.get(a));
                        ita.setAr_ec(ar_ec.get(a));
                        ita.setAr_sku(ar_sku.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }else {
                Toast.makeText(getApplicationContext(),"Maaf, tidak ada data lagi!",Toast.LENGTH_LONG).show();
            }
        }
    }

    private Boolean cekDataPkLokasi() {
        Boolean bol = false;
        for (int a=0; a<ar_pk.size(); a++){
            if(ar_lokasi.get(a)==pk_yg_diklik){
                bol=true;
                break;
            }
        }
        return bol;
    }

    private void kembaliKeHeader() {
        String lokasiUriLast="NULL";
        String lokasiUri = txtInfoLokasi;//.getText().toString();
        String lastLocationToTxt="";
        Log.d("KKH_lokasiUri",lokasiUri);
        String[] lokasiUriCut = lokasiUri.toString().trim().split("\\^");
        for (int a=0; a<lokasiUriCut.length; a++){
            if (a==lokasiUriCut.length-2){
                lokasiUriLast = String.valueOf(lokasiUriCut[a]);
            }
            if (a<lokasiUriCut.length-1){
                if (a==lokasiUriCut.length-2){
                    lastLocationToTxt += lokasiUriCut[a];
                }else {
                    lastLocationToTxt += lokasiUriCut[a] + "^";
                }
            }
        }
        Log.d("KKH_lastLocationToTxt",lastLocationToTxt);
        txtInfoLokasi = lastLocationToTxt;//.setText(lastLocationToTxt);
        Log.d("KKH_lokasiUriLast",lokasiUriLast);
        if (!lokasiUriLast.equals("NULL")){
            Log.d("KKH_lokasiUriLast","OKEH_SHOW_LOKASI_"+lokasiUriLast);
            if(Integer.parseInt(lokasiUriLast)>0){
                //munculin data WHERE lokasi_di_array == PK_nya
                Log.d("KKH_JADI","munculin data WHERE lokasi_di_array == PK_nya "+lokasiUriLast);
                adapter.items.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("KKH_JADI_FOR",""+ar_lokasi.get(a));
                    if(String.valueOf(ar_lokasi.get(a)).equals(lokasiUriLast)){
                        Log.d("KKH_JADI_FOR_",""+ar_lokasi.get(a));
                        dataClick2F ita = new dataClick2F();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_nama(ar_nama.get(a));
                        ita.setAr_ac(ar_ac.get(a));
                        ita.setAr_ec(ar_ec.get(a));
                        ita.setAr_sku(ar_sku.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
                //SET HISTORY BACK
                setHistoryDown();
                //update_text
            }else{
                //munculin data WHERE no_di_array == 0
                Log.d("KKH_JADI","munculin data WHERE no_di_array == 0");
                txtInfoLokasi = "0";//.setText("0");
                posisi_namaNow = "Home";
                txtInfoHistoriesF.setVisibility(View.GONE);
                //muncul pertama
                adapter.items.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    if(ar_no.get(a)==0){
                        dataClick2F ita = new dataClick2F();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_nama(ar_nama.get(a));
                        ita.setAr_ac(ar_ac.get(a));
                        ita.setAr_ec(ar_ec.get(a));
                        ita.setAr_sku(ar_sku.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }
        }
    }

    private void setHistoryDown() {
        String posisi_endNama = "";
        String[] hd = posisi_namaNow.toString().trim().split("\\ > ");
        for (int a=1; a<hd.length-1; a++){
            Log.d("HDHD1", hd[a]);
            posisi_endNama+= " > "+hd[a];
        }
        Log.d("HDHD2", posisi_endNama);
        posisi_namaNow = "Home "+posisi_endNama;
        txtInfoHistoriesF.setText(posisi_namaNow);
    }


    private void simpanKeArray() {
        //Simpan PK
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_pk.add(a+1);
            ar_no.add(Integer.valueOf(itemAnak[0]));
            ar_lokasi.add(0);
            ar_nama.add(itemAnak[1]);
            ar_ac.add(Integer.valueOf(itemAnak[2]));
            ar_ec.add(Integer.valueOf(itemAnak[3]));
            ar_sku.add(Integer.valueOf(itemAnak[4]));
        }
        //update Lokasi
        for (int i=0; i<ar_pk.size(); i++){
            //ar_lokasi.set(i,i+2);
            if (ar_no.get(i)==0){
                ar_lokasi.set(i,0);
            }
            if (ar_no.get(i)==1){
                ar_lokasi.set(i,1);
            }
            if(ar_no.get(i)>1) {
                if (ar_no.get(i)==no_skrg){
                    ar_lokasi.set(i,pk-1);
                }else{
                    no_skrg = ar_no.get(i);
                    pk = ar_pk.get(i);
                    ar_lokasi.set(i,pk-1);
                }
            }
        }
    }

    private void lihatDataArray() {
        for (int i=0; i<ar_no.size(); i++){
            Log.d("COBA_LAH",
                    String.valueOf(ar_pk.get(i)) + "_"+
                            String.valueOf(ar_lokasi.get(i)) + "_"+
                            String.valueOf(ar_no.get(i)) + "_"+
                            String.valueOf(ar_nama.get(i)) + "_"+
                            String.valueOf(ar_ac.get(i)) + "_" +
                            String.valueOf(ar_ec.get(i)) + "_" +
                            String.valueOf(ar_sku.get(i))
            );
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5 && keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            if (!txtInfoLokasi.equals("0")){
                kembaliKeHeader();
            }else {
                finish();
            }
            //onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public void onBackPressed() {}
}
