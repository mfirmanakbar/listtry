package com.nexsoft.firman.listtry;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ListClick2Activity extends AppCompatActivity {

    ListView list;
    AdapterClick2 adapter;
    List<dataClick2> itemList = new ArrayList<dataClick2>();

    AlertDialog.Builder dialog;
    LayoutInflater inflater;
    View dialogView;

    String dataTxt="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_click2);

        //loadClick2Data();
        //loadClick2Data2();

        list    = (ListView) findViewById(R.id.listViewClick2);

        adapter = new AdapterClick2(ListClick2Activity.this, itemList);
        list.setAdapter(adapter);

        //loadClick2Data2();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final int xno = itemList.get(position).getNo();
                final String xhuruf = itemList.get(position).getHuruf();
                final int xangka = itemList.get(position).getAngka();

                Toast.makeText(ListClick2Activity.this,"No: "+xno+", Huruf: "+xhuruf+", Angka: "+xangka,Toast.LENGTH_LONG).show();
            }
        });

        loadClick2Data2();
    }

    private void loadClick2Data2() {
        dataTxt = "0|National|60|End^1|RBH-1|50|End^2|JAKARTA GREATER|10|End^3|JAKARTA GOOD |80|End^4|TANGERANG|38|End^5|TANGERANG GT|20|End^6|PT LARISTAMA SEJAHTERA (GT)|63|End^4|JAKARTA MT|29|End^5|JAKARTA MX|95|End^6|PT LARISTAMA SEJAHTERA (MT)|34|End^4|JAKARTA WEST|47|End^5|JAKARTA GC|86|End^6|DWITUNGGAL CITRA ARYAGUNA|46|End^";
        String[] itemss = dataTxt.toString().trim().split("\\^");
        for (int a=0; a<itemss.length; a++){
            Log.d("okeajah", "item induk = " + itemss[a]);
            String[] itemss2 = itemss[a].toString().trim().split("\\|");
            /*for (int b=0; b<itemss2.length-1; b++){
                Log.d("okeajah", "item anak = " + itemss2[b]);
            }*/
            dataClick2 item = new dataClick2();
            item.setNo(Integer.parseInt(itemss2[0]));
            item.setHuruf(itemss2[1]);
            item.setAngka(Integer.parseInt(itemss2[2]));
            itemList.add(item);
            adapter.notifyDataSetChanged();
        }
    }

    /*
    private void loadClick2Data() {
        dataTxt = "" +
                "0|National|6000000000|End\n" +
                "1|RBH-1|6000000000|End\n" +
                "2|JAKARTA GREATER|6000000000|End\n" +
                "3|JAKARTA GOOD |6000000000|End\n" +
                "4|TANGERANG|2000000000|End\n" +
                "5|TANGERANG GT|2000000000|End\n" +
                "6|PT LARISTAMA SEJAHTERA (GT)|2000000000|End\n" +
                "4|JAKARTA MT|2000000000|End\n" +
                "5|JAKARTA MX|2000000000|End\n" +
                "6|PT LARISTAMA SEJAHTERA (MT)|2000000000|End\n" +
                "4|JAKARTA WEST|2000000000|End\n" +
                "5|JAKARTA GC|2000000000|End\n" +
                "6|DWITUNGGAL CITRA ARYAGUNA|2000000000|End";

        //String[] itemss = dataTxt.toString().trim().split("\\|?\\n");
        String[] itemss = dataTxt.toString().trim().split("\\n");
        for (int a=0; a<itemss.length; a++){
            Log.d("okeajah", "item = " + itemss[a]);
        }
    }*/





}
