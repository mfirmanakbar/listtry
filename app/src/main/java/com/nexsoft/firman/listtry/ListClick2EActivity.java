package com.nexsoft.firman.listtry;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ListClick2EActivity extends AppCompatActivity {

    ListView list;
    AdapterClick2E adapter;
    List<dataClick2E> itemList = new ArrayList<dataClick2E>();

    //int itemTotal, no_yg_diklik, pk_yg_diklik, lokasi_yg_diklik=0;
    int itemTotal, pk_yg_diklik, lokasi_yg_diklik=0;
    Boolean status_awal=true;

    ArrayList<Integer> ar_pk = new ArrayList<Integer>();
    ArrayList<Integer> ar_no = new ArrayList<Integer>();
    ArrayList<String> ar_huruf = new ArrayList<String>();
    ArrayList<Integer> ar_angka = new ArrayList<Integer>();
    ArrayList<Integer> ar_lokasi = new ArrayList<Integer>();

    TextView txtInfoLokasi;

    String dataTxt="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_click2);

        list    = (ListView) findViewById(R.id.listViewClick2);
        txtInfoLokasi = (TextView) findViewById(R.id.infoLokasiTxt);

        adapter = new AdapterClick2E(ListClick2EActivity.this, itemList);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pk_yg_diklik = itemList.get(position).getAr_pk();
                //no_yg_diklik = itemList.get(position).getAr_no();
                lokasi_yg_diklik = itemList.get(position).getAr_lokasi();
                //Toast.makeText(getApplicationContext(),pk_yg_diklik+"-"+lokasi_yg_diklik,Toast.LENGTH_LONG).show();
                loadClick2Data2();
            }
        });
        
        dataTxt = "1|0|0|National|60|End^2|1|1|RBH-1|50|End^3|1|1|JAKARTA GREATER|10|End^4|3|2|JAKARTA GOOD |80|End^5|3|2|TANGERANG|38|End^6|5|3|TANGERANG GT|20|End^7|5|3|PT LARISTAMA SEJAHTERA (GT)|63|End^8|5|3|JAKARTA MT|29|End^9|8|4|JAKARTA MX|95|End^10|8|4|PT LARISTAMA SEJAHTERA (MT)|34|End^11|1|1|JAKARTA GC|86|End^12|11|2|JAKARTA WEST|47|End^13|0|0|DWITUNGGAL CITRA ARYAGUNA|46|End^";
        if (dataTxt!=""){
            simpanKeArray();
            loadClick2Data2();
        }

        txtInfoLokasi.setText(lokasi_yg_diklik+"");
    }

    private void simpanKeArray() {
        String[] itemInduk = dataTxt.toString().trim().split("\\^");
        itemTotal = itemInduk.length;

        //FOR Save ke array
        for (int a=0; a<itemInduk.length; a++){
            String[] itemAnak = itemInduk[a].toString().trim().split("\\|");
            ar_pk.add(Integer.valueOf(itemAnak[0]));
            ar_lokasi.add(Integer.valueOf(itemAnak[1]));
            ar_no.add(Integer.valueOf(itemAnak[2]));
            ar_huruf.add(itemAnak[3]);
            ar_angka.add(Integer.valueOf(itemAnak[4]));
        }
    }

    private void loadClick2Data2() {
        setKeListView();
    }

    private void setKeListView() {
        if (status_awal==true){
            //muncul pertama
            adapter.items.clear();
            for (int a=0; a<ar_pk.size(); a++){
                if(ar_no.get(a)==0){
                    dataClick2E ita = new dataClick2E();
                    ita.setAr_pk(ar_pk.get(a));
                    ita.setAr_lokasi(ar_lokasi.get(a));
                    ita.setAr_no(ar_no.get(a));
                    ita.setAr_huruf(ar_huruf.get(a));
                    ita.setAr_angka(ar_angka.get(a));
                    itemList.add(ita);
                    adapter.notifyDataSetChanged();
                }
            }
            status_awal=false;
        }else {
            if (cekDataPkLokasi()==true){
                //muncul selanjutnya
                String getInfoLokasi = txtInfoLokasi.getText().toString();
                txtInfoLokasi.setText(getInfoLokasi+"^"+pk_yg_diklik+"");

                adapter.items.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("TES",a+"");
                    if(ar_lokasi.get(a)==pk_yg_diklik){
                        Log.d("TES_TRUE_IF",ar_lokasi.get(a)+"");
                        dataClick2E ita = new dataClick2E();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_huruf(ar_huruf.get(a));
                        ita.setAr_angka(ar_angka.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }else {
                Toast.makeText(getApplicationContext(),"Maaf, tidak ada data lagi!",Toast.LENGTH_LONG).show();
            }
        }
    }

    private Boolean cekDataPkLokasi() {
        Boolean bol = false;
        for (int a=0; a<ar_pk.size(); a++){
            if(ar_lokasi.get(a)==pk_yg_diklik){
                bol=true;
                break;
            }
        }
        return bol;
    }

    public void OkeClick(View view){
        //Bikin validasi kalo udah 0 maka gak boleh ke func
        //kembaliKeHeader();

        if (!txtInfoLokasi.getText().toString().equals("0")){
            kembaliKeHeader();
        }
    }

    private void kembaliKeHeader() {
        String lokasiUriLast="NULL";
        String lokasiUri = txtInfoLokasi.getText().toString();
        String lastLocationToTxt="";
        Log.d("KKH_lokasiUri",lokasiUri);
        String[] lokasiUriCut = lokasiUri.toString().trim().split("\\^");
        for (int a=0; a<lokasiUriCut.length; a++){
            if (a==lokasiUriCut.length-2){
                lokasiUriLast = String.valueOf(lokasiUriCut[a]);
            }
            if (a<lokasiUriCut.length-1){
                if (a==lokasiUriCut.length-2){
                    lastLocationToTxt += lokasiUriCut[a];
                }else {
                    lastLocationToTxt += lokasiUriCut[a] + "^";
                }
            }
        }
        Log.d("KKH_lastLocationToTxt",lastLocationToTxt);
        txtInfoLokasi.setText(lastLocationToTxt);
        Log.d("KKH_lokasiUriLast",lokasiUriLast);
        if (!lokasiUriLast.equals("NULL")){
            Log.d("KKH_lokasiUriLast","OKEH_SHOW_LOKASI_"+lokasiUriLast);
            if(Integer.parseInt(lokasiUriLast)>0){
                //munculin data WHERE lokasi_di_array == PK_nya
                Log.d("KKH_JADI","munculin data WHERE lokasi_di_array == PK_nya "+lokasiUriLast);
                adapter.items.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    Log.d("KKH_JADI_FOR",""+ar_lokasi.get(a));
                    if(String.valueOf(ar_lokasi.get(a)).equals(lokasiUriLast)){
                        Log.d("KKH_JADI_FOR_",""+ar_lokasi.get(a));
                        dataClick2E ita = new dataClick2E();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_huruf(ar_huruf.get(a));
                        ita.setAr_angka(ar_angka.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
                //update_text
            }else{
                //munculin data WHERE no_di_array == 0
                Log.d("KKH_JADI","munculin data WHERE no_di_array == 0");
                txtInfoLokasi.setText("0");
                //muncul pertama
                adapter.items.clear();
                for (int a=0; a<ar_pk.size(); a++){
                    if(ar_no.get(a)==0){
                        dataClick2E ita = new dataClick2E();
                        ita.setAr_pk(ar_pk.get(a));
                        ita.setAr_lokasi(ar_lokasi.get(a));
                        ita.setAr_no(ar_no.get(a));
                        ita.setAr_huruf(ar_huruf.get(a));
                        ita.setAr_angka(ar_angka.get(a));
                        itemList.add(ita);
                        adapter.notifyDataSetChanged();
                    }
                }
                status_awal=false;
            }
        }
    }

    private void showAllInLog() {
        //FOR untuk display aja
        for (int i=0; i<ar_no.size(); i++){
            Log.d("AYA",
                    String.valueOf(ar_pk.get(i)) + "_"+
                    String.valueOf(ar_lokasi.get(i)) + "_"+
                    String.valueOf(ar_no.get(i)) + "_"+
                    String.valueOf(ar_huruf.get(i)) + "_"+
                    String.valueOf(ar_angka.get(i))
            );
        }
    }


}
